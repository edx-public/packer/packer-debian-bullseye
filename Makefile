# default
PACKER_FILE  = packer.pkr.hcl
OUTPUT_DIR   = output
OUTPUT_NAME  = debian-11.6.0-amd64-pkr
OUTPUT_EXT   = qcow2
DATE         := $(shell date --iso)
OUTPUT       = $(OUTPUT_DIR)/$(OUTPUT_NAME)_$(DATE).$(OUTPUT_EXT)
ACCELERATOR  = kvm
PACKER_FLAGS = -var accelerator="$(ACCELERATOR)" -var output_dir="$(OUTPUT_DIR)" -var output_name="$(OUTPUT_NAME)_$(DATE).$(OUTPUT_EXT)"

# default
build: validate $(PACKER_FILE)
	packer build $(PACKER_FLAGS) $(PACKER_FILE)
	( cd $(OUTPUT_DIR) && ln -sf $(OUTPUT_NAME)_$(DATE).$(OUTPUT_EXT) $(OUTPUT_NAME)_latest.$(OUTPUT_EXT) )
	tree -h ${OUTPUT_DIR}

validate: $(PACKER_FILE)
	packer validate $(PACKER_FLAGS) $(PACKER_FILE)

clean:
	rm -rf $(OUTPUT_DIR)

really-clean: clean
	rm -rf packer_cache/
