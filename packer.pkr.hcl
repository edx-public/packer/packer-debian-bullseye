variable "source_checksum_url" {
  type    = string
  default = "file:https://cdimage.debian.org/cdimage/release/current/amd64/iso-cd/SHA256SUMS"
}

variable "source_iso" {
  description = <<EOF
* Current images in https://cdimage.debian.org/cdimage/release/
* Previous versions are in https://cdimage.debian.org/cdimage/archive/
EOF
  type    = string
  default = "https://cdimage.debian.org/cdimage/release/current/amd64/iso-cd/debian-11.6.0-amd64-netinst.iso"
}

variable "output_dir" {
  type    = string
  default = "output"
}

variable "output_name" {
  type    = string
  default = "debian.qcow2"
}

variable "ssh_password" {
  type    = string
  default = "Linux!23"
}

variable "ssh_username" {
  type    = string
  default = "sysadmin"
}

variable "accelerator" {
  type    = string
  default = "kvm"
}

# "timestamp" template function replacement
locals {
  timestamp = regex_replace(timestamp(), "[- TZ:]", "")
}


source qemu "debian" {
  iso_url            = "${var.source_iso}"
  iso_checksum       = "${var.source_checksum_url}"
  output_directory   = "${var.output_dir}"
  vm_name            = "${var.output_name}"
  cpus               = 2
  memory             = 2048
  disk_size          = "127G"
  accelerator        = "${var.accelerator}"
  headless           = true
  format             = "qcow2"
  shutdown_command   = "echo '${var.ssh_password}'  | sudo -S /sbin/shutdown -hP now"

  # Serve the `http` directory via HTTP, used for preseeding the Debian installer.
  http_directory     = "http"
  http_port_min      = 9990
  http_port_max      = 9999

  # SSH ports to redirect to the VM being built
  host_port_min      = 2222
  host_port_max      = 2229
  # This user is configured in the preseed file.
  ssh_password       = "${var.ssh_password}"
  ssh_username       = "${var.ssh_username}"
  ssh_wait_timeout   = "2h"

  # Builds a compact image
  disk_compression   = true
  disk_discard       = "unmap"
  skip_compaction    = false
  disk_detect_zeroes = "unmap"

  boot_wait          = "1s"
  boot_command       = [
    "<down><tab>",
    "preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/preseed.cfg ",
    "language=fr locale=fr_FR.UTF-8 ",
    "country=FR keymap=fr ",
    "hostname=vm-debian-t-bullseye domain=educatux.org ",
    "<enter><wait>",
  ]
}


build {
  description = <<EOF
This builder builds a QEMU image from a Debian "netinst" CD ISO file.
It contains a few basic tools and can be use as a "cloud image" alternative.
EOF

  sources = ["source.qemu.debian"]

  provisioner "file" {
    destination = "/tmp/configure-qemu-image"
    source      = "configure-qemu-image"
  }

  provisioner "shell" {
    inline = [
      "sh -cx 'sudo bash /tmp/configure-qemu-image'"
    ]
  }

  provisioner "file" {
     destination = "/tmp/zshrc.grml"
     source	= "zshrc.grml"
  }

   provisioner "file" {
     destination = "/tmp/tmux.conf"
     source	= "tmux.conf"
   }

  provisioner "shell" {
    inline = [
      "sudo cp /tmp/zshrc.grml /root/.zshrc",
      "sudo cp /tmp/zshrc.grml /home/sysadmin/.zshrc",
      "sudo cp /tmp/tmux.conf /etc/tmux.conf",
      "sudo chsh -s /bin/zsh root",
      "sudo chsh -s /bin/zsh sysadmin"
    ]
  }


  post-processor "manifest" {
    keep_input_artifact = true
    output = "packer_manifest.json"
  }

  post-processor "shell-local" {
    keep_input_artifact = true
    inline = [
      "./post-process.sh ${var.output_dir}/${var.output_name}"
    ]
  }
}
