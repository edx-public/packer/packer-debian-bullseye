#!/bin/sh
#
# Commande de Sortie de Packer pendant la création de la machine
# Executing /usr/bin/qemu-system-x86_64: []string{"-smp", "cpus=2,sockets=2", "-device", "virtio-net,netdev=user.0", "-netdev", "user,id=user.0,hostfwd=tcp::2223-:22", "-drive", "file=output/debian-11.6.0-amd64-pkr.qcow2,if=virtio,cache=writeback,discard=unmap,format=qcow2,detect-zeroes=unmap", "-drive", "file=/home/stephane/.cache/packer/588a8fbcdb7814b92d00d21914ddb4c8ad48a406.iso,media=cdrom", "-m", "2048M", "-name", "debian-11.6.0-amd64-pkr.qcow2", "-machine", "type=pc,accel=kvm", "-boot", "once=d", "-vnc", "127.0.0.1:80"}

# $(qemu-system-x86_64 -boot c -cdrom "$vyos_iso" -hda "$qemu_disk" -m "$free_ram_use" -enable-kvm -serial telnet:127.0.0.1:"$image_port",server,nowait 2>/dev/null &)

rm -f /tmp/debian.qcow2 2> /dev/null
cp -Lf ./output/debian-11.6.0-amd64-pkr_latest.qcow2 /tmp/debian.qcow2

/usr/bin/qemu-system-x86_64 -k fr -smp 2 -enable-kvm -m 2048M \
 -hda "/tmp/debian.qcow2" \
 -boot menu=off \
 -name "debian-testing"
